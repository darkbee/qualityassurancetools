﻿using System.Web;
using System.Web.Mvc;

namespace Dkb.Qa.Host.WebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
