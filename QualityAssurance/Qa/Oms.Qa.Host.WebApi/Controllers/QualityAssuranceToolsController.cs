﻿using Dkb.Qa.Host.WebApi.DI;
using Dkb.Qa.Host.WebApi.Models;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace Dkb.Qa.Host.WebApi.Controllers
{
    public class QualityAssuranceToolsController : ApiController
    {
        [HttpPost]
        [Route("sql/query")]
        public HttpResponseMessage Query(QueryRequest request)
        {
            string result = QualityAssuranceTools.ExecuteQuery(request.Table, request.Top, request.Where);
            if (!string.IsNullOrEmpty(result))
            {
                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(result, Encoding.UTF8, "application/json");
                return response;
            }
            throw new HttpResponseException(HttpStatusCode.NotFound);
        }   
    }
}