﻿using Dkb.Qa.Host.WebApi.Dynamic;
using Dkb.Qa.Host.WebApi.Extensions;
using Dkb.Qa.Host.WebApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Dkb.Qa.Host.WebApi.DI
{
    public static class QualityAssuranceTools
    {
        public static string ExecuteQuery(string table, int top = 0, string where = null)
        {
            var queryTemplate = @"SELECT {0} * FROM {1} WHERE {2}";
            var WhereParameters = MapWhereParameters(where);
            var query = string.Format(queryTemplate, (top > 0 ? "TOP " + top.ToString() : ""), table, WhereParameters.Query);
            var parameters = WhereParameters.Values.Select(s => new QueryParameters(s.Key, s.Value)).ToList();
            return JsonConvert.SerializeObject(GenericExecute(table, query, parameters), Formatting.Indented);
        }

        internal class QueryParameters
        {
            public QueryParameters(string name, object value)
            {
                this.Name = (name.IndexOf("@") == 1 ? name : "@" + name);
                this.Value = value;
            }
            public string Name { get; private set; }
            public object Value { get; private set; }
        }

        internal class WhereParameters
        {
            public WhereParameters() { }
            public WhereParameters(string query, Dictionary<string, string> values)
            {
                this.Query = query;
                this.Values = values;
            }
            public string Query { get; set; }
            public Dictionary<string, string> Values { get; set; } = new Dictionary<string, string>();
        }

        internal static WhereParameters MapWhereParameters(string where)
        {
            var result = new WhereParameters();
            var whereFinal = new StringBuilder();
            var whereList = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(where))
            {
                var regex = @"(?<identifier>:)(?<rowname>[A-Za-z0-9_\.-]*)(?<operator> >= | <= | > | < | != | = | is | in | between | like )(?<value>.[^&\|]*)(?<space>\s|)(?<aggregator>&&|\|\||)";
                var matches = Regex.Matches(where, regex, RegexOptions.IgnoreCase);
                for (int i = 0; i < matches.Count; i++)
                {
                    whereFinal.AppendFormat("{0}{1}@{2}", new object[] {
                    matches[i].Groups["rowname"].Value.Trim(),
                    matches[i].Groups["operator"].Value,
                    matches[i].Groups["rowname"].Value.Trim()
                });

                    if (matches[i].Groups["aggregator"] != null && (matches[i].Groups["aggregator"].Value == "&&" || matches[i].Groups["aggregator"].Value == "&"))
                    {
                        whereFinal.Append(" AND ");
                    }
                    else if (matches[i].Groups["aggregator"] != null && (matches[i].Groups["aggregator"].Value == "||" || matches[i].Groups["aggregator"].Value == "|"))
                    {
                        whereFinal.Append(" OR ");
                    }
                    whereList.Add(matches[i].Groups["rowname"].Value.Trim(), matches[i].Groups["value"].Value.Trim());
                }
                result.Query = whereFinal.ToString();
                result.Values = whereList;
            }
            return result;
        }

        internal static DynamicResponse GenericExecute(string tableName, string query, List<QueryParameters> parameters = null)
        {
            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DATABASE_READ_ONLY"].ToString();
            var factory = System.Data.Common.DbProviderFactories.GetFactory("System.Data.SqlClient");
            var connection = factory.CreateConnection();
            var command = factory.CreateCommand();

            parameters?.ForEach(s =>
            {
                var parameter = factory.CreateParameter();
                parameter.ParameterName = s.Name;
                parameter.Value = s.Value;
                command.Parameters.Add(parameter);
            });

            connection.ConnectionString = connectionString;
            command.CommandType = CommandType.Text;
            command.CommandTimeout = 30;
            command.CommandText = query;
            command.Connection = connection;

            connection.Open();

            var response = new DynamicResponse() { Valid = false };

            try
            {
                var reader = command.ExecuteReader();
                var dynamicData = (new DynamicData(tableName));
                var data = new List<Object>();

                var columns = Enumerable.Range(0, reader.FieldCount)
                    .Select(reader.GetName)
                    .Select((i, index) => new { Index = index, Name = i });

                if(!dynamicData.HasType(tableName))
                    foreach (var column in columns) { dynamicData.AddProperty<object>(column.Name); }

                var dataClass = dynamicData.GenerateType();

                while (reader.Read())
                {
                    object dataRow = Activator.CreateInstance(dataClass);
                    foreach (var column in columns)
                    {
                        dataRow.TrySetProperty(column.Name, reader.GetValue(column.Index));
                    }
                    data.Add(dataRow);
                }
                response.Data = data;
                response.Valid = true;
            }
            catch (Exception ex)
            {
                response.Valid = false;
                response.Error = ex.Message;
            }

            connection.Close();
            return response;
        }
    }
}