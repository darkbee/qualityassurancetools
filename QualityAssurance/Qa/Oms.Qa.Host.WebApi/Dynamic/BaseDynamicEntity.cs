﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Dkb.Qa.Host.WebApi.Dynamic
{
    public class BaseDynamicEntity : INotifyPropertyChanged
    {
        protected internal List<string> members = new List<string>();
        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string PropertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
    }
}