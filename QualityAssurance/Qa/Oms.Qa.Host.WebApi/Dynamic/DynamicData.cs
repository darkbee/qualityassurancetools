﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace Dkb.Qa.Host.WebApi.Dynamic
{
    public class DynamicData : BaseDynamicEntity
    {
        TypeBuilder TBuilder;
        static Dictionary<string, TypeBuilder> TypeBuilders { get; set; } = new Dictionary<string, TypeBuilder>();
        static Dictionary<string, Type> Types { get; set; } = new Dictionary<string, Type>();
        public bool HasType(string typeName) => Types.Any(s => s.Key.ToLower() == typeName.ToLower());

        internal static class AssemblyGen
        {
            public static readonly AssemblyName AssemblyName;
            public static readonly AssemblyBuilder Assembly;
            public static readonly ModuleBuilder Module;
            static AssemblyGen()
            {
                AssemblyName = new AssemblyName("{" + Guid.NewGuid().ToString() + "}");
                Assembly = AppDomain.CurrentDomain.DefineDynamicAssembly(AssemblyName, AssemblyBuilderAccess.Run);
                Module = Assembly.DefineDynamicModule("{" + Guid.NewGuid().ToString() + "}");
            }
        }

        public DynamicData(string typeName)
        {

            if (TypeBuilders.Any(s => s.Key.ToLower() == typeName.ToLower()))
            {
                TBuilder = TypeBuilders.Where(s => s.Key.ToLower() == typeName.ToLower()).First().Value;
            }
            else
            {
                TBuilder = AssemblyGen.Module.DefineType(typeName, TypeAttributes.Class | TypeAttributes.Public, typeof(BaseDynamicEntity));
                TypeBuilders.Add(typeName, TBuilder);
            }
        }

        public Type GenerateType()
        {
            Type result;
            var typeName = TBuilder.AsType().ToString();
            if (Types.Any(s => s.Key.ToLower() == typeName.ToLower()))
            {
                result = Types.Where(s => s.Key.ToLower() == typeName.ToLower()).FirstOrDefault().Value;
            }
            else
            {
                result = TBuilder.CreateType();
                Types.Add(typeName, result);
            }                
            return result;
        }

        public DynamicData AddProperty<T>(string key)
        {
            EmitProperty<T>(key);
            return this;
        }

        void EmitProperty<T>(string key)
        {
            if (members.Where(s => s.Equals(key, StringComparison.InvariantCultureIgnoreCase)).Count() > 0)
                return;

            var _changed = typeof(BaseDynamicEntity).GetMethod("RaisePropertyChanged", new Type[] { typeof(string) });
            var _field = TBuilder.DefineField("_" + key, typeof(T), FieldAttributes.Private);
            var _get = TBuilder.DefineMethod("get_" + key, MethodAttributes.Public, typeof(T), new Type[] { });
            var _getIL = _get.GetILGenerator();

            _getIL.Emit(OpCodes.Ldarg_0);
            _getIL.Emit(OpCodes.Ldfld, _field);
            _getIL.Emit(OpCodes.Ret);

            var _set = TBuilder.DefineMethod("set_" + key, MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.SpecialName, typeof(void), new Type[] { typeof(T) });
            var _setIL = _set.GetILGenerator();
            _setIL.Emit(OpCodes.Ldarg_0);
            _setIL.Emit(OpCodes.Ldarg_1);
            _setIL.Emit(OpCodes.Stfld, _field);

            // Raise PropertyChanged
            _setIL.Emit(OpCodes.Ldarg_0);
            _setIL.Emit(OpCodes.Ldstr, key);
            _setIL.Emit(OpCodes.Call, _changed);

            _setIL.Emit(OpCodes.Ret);

            var _property = TBuilder.DefineProperty(key, System.Reflection.PropertyAttributes.None, typeof(T), new Type[] { });

            _property.SetGetMethod(_get);
            _property.SetSetMethod(_set);

            members.Add(key);
        }
    }
}