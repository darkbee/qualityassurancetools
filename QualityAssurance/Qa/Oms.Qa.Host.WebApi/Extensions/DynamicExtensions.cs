﻿namespace Dkb.Qa.Host.WebApi.Extensions
{
    public static class DynamicExtensions
    {
        public static bool TrySetProperty(this object instance, string property, object value)
        {
            var t = instance.GetType();
            var p = t.GetProperty(property);
            if (p == null) return false;
            try { p.SetValue(instance, value, null); } catch { return false; }
            return true;
        }
    }
}