﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Web.Http;
using System.Web.Http.Filters;

namespace Dkb.Qa.Host.WebApi
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var host = $"http://{System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName()}";
            var virtualPath = System.Web.Hosting.HostingEnvironment.ApplicationHost.GetVirtualPath();

            // Web API configuration and services
            var formatters = GlobalConfiguration.Configuration.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;
            // serialize dates into ISO format with UTC timezone
            settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            settings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            RegisterWebApiFilters(GlobalConfiguration.Configuration.Filters);

            GlobalConfiguration.Configuration.DependencyResolver = new WebApiResolver();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        private static void RegisterWebApiFilters(HttpFilterCollection filter)
        {
  
        }
    }
}