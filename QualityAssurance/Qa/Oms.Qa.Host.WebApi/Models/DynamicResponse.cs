﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dkb.Qa.Host.WebApi.Models
{
    [DataContract]
    public class DynamicResponse
    {
        [DataMember(Name = "data")]
        public List<Object> Data { get; set; } = new List<Object>();

        [DataMember(Name = "valid")]
        public bool Valid { get; set; }

        [DataMember(Name = "error")]
        public string Error { get; set; }
    }
}