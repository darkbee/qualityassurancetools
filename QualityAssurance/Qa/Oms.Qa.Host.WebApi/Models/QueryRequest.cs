﻿namespace Dkb.Qa.Host.WebApi.Models
{
    public class QueryRequest
    {
        public string Table { get; set; }
        public int Top { get; set; }
        public string Where { get; set; }
    }
}